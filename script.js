const resetButton = document.getElementById("reset");
let winner = document.getElementById("winner");
let vs = document.getElementById("vs");
const choices = [
  { choice: "rock", comChoice: "comRock", beats: "scissors", loseTo: "paper" },
  { choice: "paper", comChoice: "comPaper", beats: "rock", loseTo: "scissors" },
  {
    choice: "scissors",
    comChoice: "comScissors",
    beats: "paper",
    loseTo: "rock",
  },
];
// function to change background when clicked
const background = () => {
  // player background
  choices.forEach((ele) => {
    let playerId = document.getElementById(ele.choice);
    playerId.onclick = () => {
      playerId.style.backgroundColor = "rgba(196, 196, 196, 1)";
    };
  });
  // computer background
  choices.forEach((ele) => {
    let comId = document.getElementById(ele.comChoice);
    comId.style.backgroundColor = "";
  });
};
// Generate random computer choice
const getRandomInt = () => {
  return Math.floor(Math.random() * 3);
};
// Function winner visibility
const visibility = () => {
  vs.style.visibility = "hidden";
  winner.style.visibility = "visible";
};
// Function to Determine winner and loser
let player2Choice = choices[getRandomInt()].choice;
const gameRound = (id) => {
  let idIndex = choices.map((item) => item.choice).indexOf(id);

  if (id === choices[idIndex].choice) {
    if (choices[idIndex].loseTo.includes(player2Choice)) {
      winner.textContent = "COM WIN";
      visibility();
      winner.style.backgroundColor = "#4c9654";
    } else if (choices[idIndex].beats.includes(player2Choice)) {
      winner.textContent = "PLAYER 1 WIN";
      visibility();
      winner.style.backgroundColor = "#4c9654";
    } else {
      winner.textContent = "DRAW";
      visibility();
      winner.style.backgroundColor = "#035B0C";
    }
  }
};
// Return index of player
const idToNumber = (id) => {
  switch (id) {
    case "rock":
      return 0;
    case "paper":
      return 1;
    case "scissors":
      return 2;
  }
};
// Change computer background color
const comBackground = () => {
  let comId = document.getElementById(
    choices[idToNumber(player2Choice)].comChoice
  );
  comId.style.backgroundColor = "rgba(196, 196, 196, 1)";
};
// function to reset background
const resetBackground = () =>
  choices.forEach((ele) => {
    let playerId = document.getElementById(ele.choice);
    playerId.style.backgroundColor = "";
  });
// Make class object
class Option {
  constructor(id1, id2, id3) {
    this.id1 = id1;
    this.id2 = id2;
    this.id3 = id3;
  }
  getClicked() {
    let clicked = false;
    document.getElementById(this.id1).onclick = () => {
      if (!clicked) {
        clicked = true;
        document.getElementById(this.id1).style.backgroundColor =
          "rgba(196, 196, 196, 1)";
        document.getElementById(this.id2).onclick = null;
        document.getElementById(this.id3).onclick = null;
        comBackground();
        gameRound(this.id1); //this function is called to determine winner and loser
        player2Choice = choices[getRandomInt()].choice;
      }
    };
  }
}
// Create new object and call methods in object class
let rock = new Option("rock", "paper", "scissors");
let paper = new Option("paper", "rock", "scissors");
let scissors = new Option("scissors", "paper", "rock");
// Reset all parameters to default values
let reset = () =>
  (resetButton.onclick = () => {
    resetBackground();
    background();
    rock.getClicked();
    paper.getClicked();
    scissors.getClicked();
    winner.style.visibility = "hidden";
    vs.style.visibility = "visible";
  });

rock.getClicked();
paper.getClicked();
scissors.getClicked();
reset();
